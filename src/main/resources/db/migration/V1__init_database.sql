CREATE TABLE news (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  content TEXT,
  PRIMARY KEY (id),
  FULLTEXT (name, content)
);