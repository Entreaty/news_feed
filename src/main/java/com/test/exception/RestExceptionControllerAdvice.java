package com.test.exception;

import com.test.model.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.Iterator;

@ControllerAdvice(annotations = RestController.class)
public class RestExceptionControllerAdvice {

	private static final Logger logger = LoggerFactory.getLogger(RestExceptionControllerAdvice.class);

	/**
	 * Exception handler
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		logger.error("Rest error  = ", ex);
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage("Please contact your administrator");
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ApiException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(ApiException ex) {
		logger.error("Rest ApiException  = ", ex);
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.BAD_REQUEST.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Missing param exception
	 * errorCode: 400,
	 * message: "Required (type of param) parameter 'name of param' is not present"
	 */
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(MissingServletRequestParameterException ex) {
		logger.error("Rest MissingServletRequestParameterException  = ", ex);
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.BAD_REQUEST.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Returns wrong params
	 * errorCode: 400,
	 * message: "name content category" - if missed three parameters
	 */
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErrorResponse> constraintViolationHandler(ConstraintViolationException e) {
		logger.error("Rest ConstraintViolationException = ", e);
		String message = "";
		for (ConstraintViolation constraintViolation : e.getConstraintViolations()) {
			Iterator<Path.Node> iterator = constraintViolation.getPropertyPath().iterator();
			while (iterator.hasNext()) {
				Path.Node node = iterator.next();
				if (!iterator.hasNext()) {
					message += node.getName() + " ";
				}
			}
		}
		// Remove last space character
		if (message.length() > 0) {
			message = message.substring(0, message.length() - 1);
		}
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.BAD_REQUEST.value());
		error.setMessage(message);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}


}