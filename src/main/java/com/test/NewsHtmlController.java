package com.test;

import org.springframework.web.bind.annotation.RequestMapping;

@HtmlController
public class NewsHtmlController {

	@RequestMapping("/news-feed")
	public String index() {
		return "index";
	}
}
