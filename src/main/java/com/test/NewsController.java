package com.test;

import com.test.dao.CategoryDao;
import com.test.dao.NewsDao;
import com.test.dao.NewsRepository;
import com.test.exception.ApiException;
import com.test.model.Category;
import com.test.model.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping("/news")
public class NewsController {
	/**
	 * * POST create news
	 *
	 * @param name     minSize = 4
	 * @param content  minSize = 4
	 * @param category minSize = 1
	 * @return News
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public News create(@Size(min = 4) String name,
					   @Size(min = 4) String content,
					   @Size(min = 1) String category) {
		Category categoryObj = getCategoryByName(category);
		News news = new News(name, content, categoryObj);
		newsDao.save(news);
		return news;
	}

	/**
	 * POST Delete news by id
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public void delete(@NotNull long id) {
		try {
			newsDao.delete(id);
		} catch (EmptyResultDataAccessException exception) {
			throw new ApiException("The news must not be null!");
		}
	}

	/**
	 * GET Get all news
	 *
	 * @param offset
	 * @param limit
	 * @param word       - add filter by name and content
	 * @param categoryId - add filter by category
	 * @return
	 */
	@RequestMapping("/news")
	@ResponseBody
	public List<News> getAllNews(@NotNull int offset, @NotNull int limit, @Size(min = 4) String word, String categoryId) {
		return newsRepository.getAll(offset, limit, word, categoryId);
	}

	/**
	 * GET Get all categories
	 */
	@RequestMapping("/categories")
	@ResponseBody
	public List<Category> getAllCategories() {
		return categoryDao.findAll();
	}

	/**
	 * GET Get all news and categories
	 */
	@RequestMapping("/news-and-categories")
	@ResponseBody
	public Map<String, List> getAll(@NotNull int offset, @NotNull int limit) {
		Map<String, List> map = new HashMap<>();
		map.put("news", getAllNews(offset, limit, "", ""));
		map.put("categories", getAllCategories());
		return map;
	}

	/**
	 * GET Get one news by id
	 */
	@RequestMapping("/id")
	@ResponseBody
	public News getById(@NotNull long id) {
		return newsDao.findOne(id);
	}

	/**
	 * POST Update the news
	 *
	 * @param id
	 * @param name     minSize = 4
	 * @param content  minSize = 4
	 * @param category minSize = 1
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public void updateUser(@NotNull long id,
						   @Size(min = 4) String name,
						   @Size(min = 4) String content,
						   @Size(min = 1) String category) {
		Category categoryObj = getCategoryByName(category);
		newsDao.updateNews(id, name, content, categoryObj.getId());
	}

	/** Private fields */
	@Autowired
	private NewsDao newsDao;
	@Autowired
	private CategoryDao categoryDao;
	@Autowired
	private NewsRepository newsRepository;

	/**
	 * Returns Category object
	 */
	private Category getCategoryByName(String category) {
		Category categoryObj = categoryDao.findOneByName(category);
		if (categoryObj == null) {
			categoryObj = new Category(category);
			categoryDao.save(categoryObj);
		}
		return categoryObj;
	}
}
