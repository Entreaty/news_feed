package com.test.dao;


import com.test.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface CategoryDao extends JpaRepository<Category, Long> {


	Category findOneByName(String name);
}