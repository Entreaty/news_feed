package com.test.dao;

import com.test.model.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;


@Transactional
public interface NewsDao extends JpaRepository<News, Long> {

	@SuppressWarnings("JpaQlInspection")
	@Modifying
	@Query(nativeQuery = true, value = "update news n set n.name = ?2, n.content = ?3, n.category_id = ?4 where n.id = ?1")
	void updateNews(Long id, String name, String content, long category);

}
