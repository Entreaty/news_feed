package com.test.dao;

import com.test.model.News;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Transactional(readOnly = true)
public class NewsRepositoryImpl implements NewsRepositoryCustom {
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<News> getAll(int offset, int limit, String word, String categoryId) {
		if (limit > 50) limit = 50;
		String a = " 1 AND ", b = " 1 ";
		boolean categoryExist = categoryId != null && !categoryId.equals("");
		boolean searchWordExist = word != null && !word.equals("");
		if (categoryExist) {
			a = "category_id = ?3 AND ";
		}
		if (searchWordExist) {
			b = " MATCH(name,content) AGAINST(?4 IN BOOLEAN MODE) ";
		}
		Query query = entityManager.createNativeQuery("SELECT * FROM news WHERE " + a + b + " LIMIT ?1,?2 ", News.class);
		query.setParameter(1, offset);
		query.setParameter(2, limit);
		if (categoryExist) query.setParameter(3, categoryId);
		if (searchWordExist) query.setParameter(4, word);
		return query.getResultList();
	}
}
