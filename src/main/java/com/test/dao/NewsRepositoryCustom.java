package com.test.dao;

import com.test.model.News;

import java.util.List;

public interface NewsRepositoryCustom {

	List<News> getAll(int offset, int limit, String word, String categoryId);

}
